module X64 where

import Opts

import Data.Word


data X64Inst =
  X64Inst {
    xPrefixes :: ![String] -- e.g. rep, xlock
  , xMnemonic :: !String
  , xOperands :: ![String]
  } deriving (Show,Eq)

data X64Operand =
    X64OperandREG !X64Reg
  | X64OperandIMM !Word64 -- jmp         000000018000102B
  | X64OperandADDR ![Maybe X64Reg] !Int !Word64 -- [rax+10h]
  deriving (Show,Eq,Ord)

data X64Reg =
  RAX
  deriving (Show,Eq,Ord,Enum)


-- https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/x64-architecture
-- first four integer args in: rcx, rdx, r8, r9
-- first four floating point in: xmm0...xmm3
-- (then onto the stack)
-- return is rax (int) or xmm0 (float)
-- https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/annotated-x64-disassembly
--
x64_inst_prefixes :: [String]
x64_inst_prefixes = ["lock", "rep", "xacquire", "xrelease"]

x64_regs :: [String]
x64_regs =
    -- 64b: rax, ... rsi, ... r8, ...
    map (\r -> "r" ++ r ++ "x") abcd ++ map ("r" ++) spec ++ rnum ++
    -- 32b: eax, ... esi, ... r8d, ...
    map (\r -> "e" ++ r ++ "x") abcd ++ map ("e" ++) spec ++ map (++ "d") rnum ++
    -- 16b: ax, ... si ..., r8w
    map (\r -> r ++ "x") abcd ++ spec ++ map (++ "w") rnum ++
    -- 8b: al, ... sil ..., r8b
    map (\r -> r ++ "l") abcd ++ map (++ "l") spec ++ map (++ "b") rnum ++
    -- 8b high
    map (\r -> r ++ "h") abcd ++
    -- rip, eip (32b)
    ips ++
    -- xmm0..xmm3
    xmm
  where abcd = ["a", "b", "c", "d"]
        spec = ["si", "di", "bp", "sp"]
        rnum = ["r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "r16"]
        ips = ["rip", "eip"]
        xmm = ["xmm" ++ show r | r <- [0..3]]
