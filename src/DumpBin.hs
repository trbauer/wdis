module DumpBin where

import Opts

import Control.Monad
import Data.List
import System.Directory
import System.Environment
import System.IO


-- EXAMPLES
-- % dumpbin /exports Hello.dll
-- % dumpbin /disasm /section:.text Hello.dll
-- % dumpbin /rawdata /section:.rodata Hello.dll

-- or "" if not found
findDumpBin :: Handle -> Opts -> IO FilePath
findDumpBin h_oup os = do
  menv <- lookupEnv dumpbin_env
  case menv of
    Just exe -> do
      oVerboseLnH h_oup os $ "== dumpbin.exe overridden via %" ++ dumpbin_env ++ "% to " ++ exe
      z <- doesFileExist exe
      unless z $
        fatal "dumpbin.exe referenced does not exist"
      return exe
    Nothing -> do
      dbs <- findDumpBins h_oup os
      case dbs of
        [] -> fatal "cannot find dumpbin.exe (is Visual Studio installed; -v2 for verbose search)"
        db:_ -> do
          oVerboseLnH h_oup os $ "== dumpbin.exe found at " ++ db
          return db

findDumpBins :: Handle -> Opts -> IO [FilePath]
findDumpBins h_oup os = do
    search [] search_dirs
  where search rps [] = return (reverse rps)
        search rps (d:ds) = do
          oDebugLnH h_oup os $ "== searching " ++ d
          z <- doesDirectoryExist d
          if not z then search rps ds
            else do
              vers <- reverse . sort <$> listDirectory d
              -- e.g. has "14.38.33130" (in highest version first lexicographically)
              -- for each version V
              let ds = map (\ver -> d ++ ver ++ "\\bin\\Hostx64\\x64\\dumpbin.exe") vers
              bs <- filterM doesFileExist ds
              search (reverse bs ++ rps) ds

search_dirs :: [FilePath]
search_dirs =
  [
    "C:\\Program Files\\Microsoft Visual Studio\\2022\\Enterprise\\VC\\Tools\\MSVC\\"
  , "C:\\Program Files\\Microsoft Visual Studio\\2022\\Professional\\VC\\Tools\\MSVC\\"
  , "C:\\Program Files\\Microsoft Visual Studio\\2022\\Community\\VC\\Tools\\MSVC\\"
  , "C:\\Program Files\\Microsoft Visual Studio\\2019\\Enterprise\\VC\\Tools\\MSVC\\"
  , "C:\\Program Files\\Microsoft Visual Studio\\2019\\Professional\\VC\\Tools\\MSVC\\"
  , "C:\\Program Files\\Microsoft Visual Studio\\2019\\Community\\VC\\Tools\\MSVC\\"
  ]
