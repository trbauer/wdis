module Opts where

import Color

import Control.Monad
import Data.Char
import Data.List
import System.Exit
import System.IO


wdis_version :: String
wdis_version = "0.0.0"


printUsage :: IO ()
printUsage =
  putStrLn $
    "usage: wdis OPTS INPUT\n" ++
    "Windows Disassembler - " ++ wdis_version ++ "\n" ++
    "where OPTS are:\n" ++
    "   --color=on|off|auto        enables color (auto selects based on TTY)\n" ++
    "   -e/--exports               dumps exports\n" ++
    "   --list-dumpbins            finds dumpbin.exe\n" ++
    "   -o=../--output=PATH        specifies output path (default is stdout)\n" ++
    "   -q/-v/-v2/-v=INT           sets the verbosity\n" ++
    "ENVIRONMENT: setting %" ++ dumpbin_env ++ "% overrides version\n" ++
    "\n" ++
    "EXAMPLES:\n" ++
    "  % wdis  File.dll\n" ++
    ""

dumpbin_env :: String
dumpbin_env = "DUMPBIN_EXE"


data Opts =
  Opts {
    oColor :: !Color
  , oDumpExports :: !Bool
  , oInputs :: ![FilePath]
  , oListDumpBins :: !Bool
  , oOutput :: !FilePath
  , oVerbosity :: !Int
  } deriving (Show,Eq)
data Color = ColorON | ColorOFF | ColorAUTO deriving (Eq,Show)


dft_opts :: Opts
dft_opts =
  Opts {
    oColor = ColorAUTO
  , oDumpExports = False
  , oInputs = []
  , oListDumpBins = False
  , oOutput = ""
  , oVerbosity = 0
  }


parseOpts :: Opts -> [String] -> IO Opts
parseOpts os [] = return os
parseOpts os (a:as)
  | a `elem` ["-h","--help"] = printUsage >> exitSuccess
  --
  | a == "-q" = nextArg os{oVerbosity = -1}
  | a == "-v" = nextArg os{oVerbosity = 1}
  | a`elem`["-v2"] = nextArg os{oVerbosity = 2}
  | k == "-v=" = parseAsIntValue (\i -> os{oVerbosity = i})
  --
  | k == "--color=" =
    case v of
      "on" -> nextArg os {oColor = ColorON}
      "off" -> nextArg os {oColor = ColorOFF}
      "auto" -> nextArg os {oColor = ColorAUTO}
      _ -> badArg $ v ++ ": invalid value for --color"
  | k`elem`["-o=","--output="] =
    if null (oOutput os) then nextArg os {oOutput = v}
      else badArg "--output already specified"
  | a == "--list-dumpbins" = nextArg os {oListDumpBins = True}
  | a `elem`["-e","--exports"] = nextArg os {oDumpExports = True}
  | "-"`isPrefixOf`a = badArg "invalid option"
  --
  | otherwise = nextArg os {oInputs = oInputs os ++ [a]}
  where (k,v) =
          case span (/='=') a of
            (k,'=':sfx) -> (k ++ "=",sfx)
            (inp,"") -> (inp,"")

        nextArg :: Opts -> IO Opts
        nextArg os = parseOpts os as

        parseAsIntValue :: (Int -> Opts) -> IO Opts
        parseAsIntValue func =
          case reads v of
            [(x,"")] -> parseOpts (func x) as
            _ -> badArg "malformed integer"
        badArg msg = die (a ++ ": " ++ msg)

fatal :: String -> IO a
fatal = die

-- oFatalLn :: Opts -> String -> IO ()
-- oFatalLn = oIoLevel putStrLnRed (-2)
-- oWarningLn :: Opts -> String -> IO ()
-- oWarningLn = oIoLevel putStrLnYellow (-1)
oNormalLnH :: Handle -> Opts -> String -> IO ()
oNormalLnH h = oIoLevel (hPutStrLn h) 0
oVerboseLnH :: Handle -> Opts -> String -> IO ()
oVerboseLnH h = oIoLevel (hPutStrLn h) 1
oVerboseH :: Handle -> Opts -> String -> IO ()
oVerboseH h = oIoLevel (hPutStr h) 0
oDebugLnH :: Handle -> Opts -> String -> IO ()
oDebugLnH h = oIoLevel (hPutStrLn h) 2
oIoLevel :: (String -> IO ()) -> Int -> Opts -> String -> IO ()
oIoLevel io lvl os
  | oVerbosity os >= lvl = io
  | otherwise = const (return ())
