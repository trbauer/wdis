module Color where

import qualified System.Console.ANSI as SCA -- cabal install ansi-terminal

import Control.Exception
import System.IO


putStrRed :: String -> IO ()
putStrRed = hPutVivid SCA.Red stdout
hPutStrRed :: Handle -> String -> IO ()
hPutStrRed = hPutVivid SCA.Red
--
putStrDarkRed :: String -> IO ()
putStrDarkRed = hPutStrDarkRed stdout
hPutStrDarkRed :: Handle -> String -> IO ()
hPutStrDarkRed = hPutDull SCA.Red
--
--
putStrGreen :: String -> IO ()
putStrGreen = hPutStrGreen stdout
hPutStrGreen :: Handle -> String -> IO ()
hPutStrGreen = hPutVivid SCA.Green
--
putStrDarkGreen :: String -> IO ()
putStrDarkGreen = hPutStrDarkGreen stdout
hPutStrDarkGreen :: Handle -> String -> IO ()
hPutStrDarkGreen = hPutDull SCA.Green
--
--
putStrBlue :: String -> IO ()
putStrBlue = hPutStrBlue stdout
hPutStrBlue :: Handle -> String -> IO ()
hPutStrBlue = hPutVivid SCA.Blue
--
putStrDarkBlue :: String -> IO ()
putStrDarkBlue = hPutStrDarkBlue stdout
hPutStrDarkBlue :: Handle -> String -> IO ()
hPutStrDarkBlue = hPutDull SCA.Blue
--
--
putStrWhite :: String -> IO ()
putStrWhite = hPutVivid SCA.White stdout
hPutStrWhite :: Handle -> String -> IO ()
hPutStrWhite = hPutVivid SCA.White
--
--
putStrYellow :: String -> IO ()
putStrYellow = hPutStrYellow stdout
hPutStrYellow :: Handle -> String -> IO ()
hPutStrYellow = hPutVivid SCA.Yellow
--
putStrDarkYellow :: String -> IO ()
putStrDarkYellow = hPutStrDarkYellow stdout
hPutStrDarkYellow :: Handle -> String -> IO ()
hPutStrDarkYellow = hPutDull SCA.Yellow
--
--
putStrCyan :: String -> IO ()
putStrCyan = hPutStrCyan stdout
hPutStrCyan :: Handle -> String -> IO ()
hPutStrCyan = hPutVivid SCA.Cyan
--
putStrDarkCyan :: String -> IO ()
putStrDarkCyan = hPutStrDarkCyan stdout
hPutStrDarkCyan :: Handle -> String -> IO ()
hPutStrDarkCyan = hPutDull SCA.Cyan
--
--
putStrMagenta :: String -> IO ()
putStrMagenta = hPutStrMagenta stdout
hPutStrMagenta :: Handle -> String -> IO ()
hPutStrMagenta = hPutVivid SCA.Magenta
--
putStrDarkMagenta :: String -> IO ()
putStrDarkMagenta = hPutStrDarkMagenta stdout
hPutStrDarkMagenta :: Handle -> String -> IO ()
hPutStrDarkMagenta = hPutDull SCA.Magenta
--
--
hPutVivid :: SCA.Color -> Handle -> String -> IO ()
hPutVivid = hPutColor SCA.Vivid
hPutDull :: SCA.Color -> Handle -> String -> IO ()
hPutDull = hPutColor SCA.Dull
hPutColor :: SCA.ColorIntensity -> SCA.Color -> Handle -> String -> IO ()
hPutColor i c h str = bracket_ acq rel act
  where acq = hFlush h >> SCA.hSetSGR h [SCA.SetColor SCA.Foreground i c]
        act = hPutStr h str
        rel = SCA.hSetSGR h [SCA.Reset]
