module Main where

import qualified Driver as D

import System.Environment(getArgs)

main :: IO ()
main = getArgs >>= D.run
