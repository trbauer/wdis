module Driver where

import Color
import DumpBin
import Opts
import X64

import Control.Monad
import Data.Char
import Data.List
import Data.Word
import Debug.Trace
import System.Process
import System.Exit
import System.IO
import Text.Printf


--
-- TODO:
-- * resolve imports? run /IMPORTS and parse them
--    test tinylib to figure that out
-- *

run :: [String] -> IO ()
run as = parseOpts dft_opts as >>= runWithOpts

runWithOpts :: Opts -> IO ()
runWithOpts os
  | null (oInputs os) && not (oListDumpBins os) = fatal "expected input (or --list-dumpbins)"
  | not (null (oInputs os)) && oListDumpBins os = fatal "--list-dumpbins mutually exclusive with other inputs"
  | null (oOutput os) = withOutput stdout
  | otherwise = withFile (oOutput os) WriteMode withOutput
  where withOutput h = do
          oDebugLnH h os $ show os
          is_tty <- hIsTerminalDevice h
          runWithOptsH h
            os {
              oColor = if oColor os /= ColorAUTO then oColor os
                         else if is_tty then ColorON else ColorOFF
            }

runWithOptsH :: Handle -> Opts -> IO ()
runWithOptsH h_oup os
  | oListDumpBins os = do
    findDumpBin h_oup os {oVerbosity = 2} -- pump up the verbosity
    return ()
  | otherwise = do
    dumpbin_exe <- findDumpBin h_oup os
    mapM_ (disFileAsync h_oup dumpbin_exe os) (oInputs os)


--    ordinal hint RVA      name
--
--          1    0 00002840 clBuildProgram
--          2    1 000032A0 clCloneKernel
--
--        122   79 00002FA0 clUnloadPlatformCompiler
--        123   7A 00002980 clWaitForEvents
parseExports :: [String] -> [(Word64,String)]
parseExports =
    loop . drop 2 . dropWhile ((/=["ordinal", "hint", "RVA", "name"]) . words)
   where loop :: [String] -> [(Word64,String)]
         loop [] = []
         loop (ln:lns)
           -- | trace (" * " ++ show ln) False = undefined
           | all isSpace ln = []
           | otherwise =
            case words ln of
              [ord,hint,rva_str,name] ->
                case reads ("0x" ++ rva_str) of
                  [(rva,"")] -> (rva, name):loop lns
                  _ -> []
              _ -> []

    -- out <- callExe os dumpbin_exe ["/DISASM:WIDE"]


-- parse /HEADERS
-- parse /EXPORTS
--   180000000 image base (0000000180000000 to 000000018016BFFF)

callExe :: Handle -> Opts -> FilePath -> [String] -> String -> IO String
callExe h_out os exe as inp = do
  let esc_arg a
        | any isSpace a = "\"" ++ a ++ "\""
        | otherwise = a
  oDebugLnH h_out os $ " % " ++ esc_arg exe ++ " " ++ intercalate " " (map esc_arg as)
  readProcess exe as inp

-- RVA is relative to the DLL load address (beginning of DLL)
-- /DUMPBIN /SECTION:_RDATA
-- /DUMPBIN /SECTION:.rsrc

disFileAsync :: Handle -> FilePath -> Opts -> FilePath -> IO ()
disFileAsync h_oup dumpbin_exe os inp_fp = do
  if oDumpExports os then do
      out <- callExe h_oup os dumpbin_exe ["/EXPORTS", inp_fp] ""
      hPutStr h_oup out
    else do
      hdrs_exps_lns <- lines <$> callExe h_oup os dumpbin_exe ["/HEADERS", "/EXPORTS", inp_fp] ""
      let m_image_base :: Maybe Word64
          m_image_base =
            case filter ("image base ("`isInfixOf`) hdrs_exps_lns of
              --       180000000 image base (0000000180000000 to 000000018016BFFF)
              x:_ ->
                case words x of
                  hex_str:_ ->
                    case reads ("0x" ++ hex_str) of
                      [(hex,"")] -> Just hex
                  [] -> Nothing
              [] -> Nothing
          exps = parseExports hdrs_exps_lns
      -- putStrLn $ unlines hdrs_exps_lns

      oVerboseLnH h_oup os $
        case m_image_base of
          Just image_base -> printf "image base: 0x%016X" image_base
          _ -> "image base: ?"
      oVerboseLnH h_oup os $
        "EXPORTS:\n" ++
        concatMap (\(rva,exp) -> printf "  * %012X " rva ++ exp ++ "\n") exps


      -- We have to do this with pipes because the output could be
      -- really really large and readProcess is synchronous on the output
      -- (at least as of this writing).
      let cp = (proc dumpbin_exe ["/DISASM:NOBYTES", inp_fp]) {
                 std_out = CreatePipe
               , std_err = Inherit
               , std_in = NoStream
               }
      (_,Just h_dumbin_out,_,ph) <- createProcess cp
      case m_image_base of
        Just image_base -> do
          disTextAsync h_oup os image_base exps h_dumbin_out
          ec <- waitForProcess ph
          case ec of
            ExitFailure ec -> fatal $ show ec ++ "child process failed"
            ExitSuccess -> return ()



-- PROBLEM: to insert my own labels (as targets) I need to parse all uses/calls first
disTextAsync :: Handle -> Opts -> Word64 -> [(Word64,String)] -> Handle -> IO ()
disTextAsync h_oup os image_base exps h_inp = skipToAsm
  where skipToAsm :: IO ()
        skipToAsm = do
          ln <- hGetLine h_inp
          if not ("File Type: DLL"`isPrefixOf` ln)
            then skipToAsm
            else do
              skipLines 1
              loopAsm

        loopAsm :: IO ()
        loopAsm = do
          ln <- hGetLine h_inp
          if all isSpace ln then return ()
            else loopAsmLn h_oup os image_base exps ln >> loopAsm

        skipLines :: Int -> IO ()
        skipLines 0 = return ()
        skipLines n = do
          ln <- hGetLine h_inp
          skipLines (n - 1)

--
-- 000180004CBC: lea         rax,[0000000181A59670h]
-- 000180004BFF: movups      xmm0,xmmword ptr [rsp+30h]
-- 000000018040E022: lea         rdi,[rcx+rax*8]
-- 00000001804500DB: cmp         dword ptr [rcx+rdx*8-8],eax
testHighlight :: String -> IO ()
testHighlight ln = loopAsmLn stdout dft_opts{oColor = ColorON} 0 [] ln

loopAsmLn :: Handle -> Opts -> Word64 -> [(Word64,String)] -> String -> IO ()
loopAsmLn h_oup os image_base exps ln = do
      -- 0000000180001000: 48 89 4C 24 08                               mov         qword ptr [rsp+8],rcx
      -- 0000000180001000: mov         qword ptr [rsp+8],rcx
      -- 000000018000109E: jmp         qword ptr [0000000180002010h]
      -- 0000000180002DFE: lock xadd   qword ptr [0000000184001730h],rax
      -- 0000000180002E07: and         dword ptr [0000000183F144BCh],0FFFFF000h
      -- 000000018136069D: rep xor     eax,35F34001h
      -- 0000000181360695: xacquire xor eax,35F30601h
      case reads ("0x" ++ dropWhile isSpace ln) of
        [(addr,':':' ':sfx)] -> do
          -- 0000000180001060: ...
          -- but image base is 180001060 and
          --    ordinal hint RVA      name
          --
          -- 1    0 00001060 tinylib_print
          -- 2    1 00001040 tinylib_thrice
          --
          -- So, before this line we would place tinylib_print
          --   0000000180001060: 48 89 4C 24 08     mov         qword ptr [rsp+8],rcx
          let lbls = filter (\(exp_rva,_) -> exp_rva + image_base == addr) exps
          forM_ lbls $ \(_,sym) -> putLbl (sym ++ ":\n")
          _ <- processSpaces ln -- process *after* emitting labels
          putMag $ printf "%012X: " addr
          processPfxOrMne addr [] sfx
        _ -> do
          putError ln
          -- fatal ("assembly parse error (address)\n" ++ ln)
    where processSpaces :: String -> IO String
          processSpaces s =
            case span isSpace s of
              (spcs,sfx) -> putNormal spcs >> return sfx

          processPfxOrMne :: Word64 -> [String] -> String -> IO ()
          processPfxOrMne addr mne_tks sfx = do
            sfx <- processSpaces sfx
            case span isAlphaNum sfx of
              (mne,sfx) -> do
                if mne`elem`x64_inst_prefixes
                  then putMne mne >> processPfxOrMne addr (mne:mne_tks) sfx
                  else do
                    putMne mne
                    sfx <- processSpaces sfx
                    parseOpnd addr (mne:mne_tks) 0 sfx

          parseOpnd :: Word64 -> [String] -> Int -> String -> IO ()
          parseOpnd addr mne_tks op_ix "" = putNormalC '\n' -- any operand-less instructions?
          parseOpnd addr mne_tks op_ix sfx = do
            -- lea         rcx,[0000000180002020h]
            let (tk,tk_sfx) = span (\c ->  c`notElem`",") sfx
            case words tk of
              ['[':sfx@(c:cs)] | sfx_last == ']'  -> do
                    putNormalC '[' >> putAddrExpr body >> putNormalC ']'
                  where (body,sfx_last) = (init sfx,last sfx)
              [ty,"ptr",'[':sfx] -> do
                putNormal (ty ++ " ptr [")
                case span (/=']') sfx of
                  (pfx,"") -> putStrRed pfx
                  (pfx,']':sfx) -> putAddrExpr pfx >> putNormalC ']'
              _
                | tk`elem`x64_regs -> putReg tk
                | isNumToken tk -> putImm tk
              _ -> putNormal tk
            case tk_sfx of
              ',':sfx -> putNormalC ',' >> parseOpnd addr mne_tks (op_ix + 1) sfx
              "" -> parseOpnd addr mne_tks (op_ix + 1) ""

          -- Stuff inside [...] from lea or ptr loads
          -- 0000000181A59670h
          -- rsp+30h
          -- rcx+rax*8
          -- rcx+rdx*8-8
          putAddrExpr :: String -> IO ()
          putAddrExpr = loopT . splitTerms
            where loopT :: [[String]] -> IO ()
                  loopT ts =
                    case ts of
                      [] -> return ()
                      [t] -> loopF t
                      t:[op]:ts -> loopF t >> putNormal op >> loopT ts
                      -- _ -> could mean [op] fails, but that means splitTerms
                      -- is busted; operators should be alone

                  loopF :: [String] -> IO ()
                  loopF fs =
                    case fs of
                      [] -> return ()
                      [f] -> loopP f
                      f:op:fs -> loopP f >> putNormal op >> loopF fs

                  loopP :: String -> IO ()
                  loopP a
                    | a`elem`x64_regs = putReg a
                    | isNumToken a = putImm a
                    | otherwise = putError a

          isNumToken :: String -> Bool
          isNumToken tk =
            isHexToken tk || isUnsuffixedHexToken tk || isDecToken tk
          isHexToken :: String -> Bool
          isHexToken s =
            case span isHexDigit s of
              (ds@(_:_),"h") -> True
              _ -> False
          isUnsuffixedHexToken :: String -> Bool
          isUnsuffixedHexToken s =
            case span isHexDigit s of
              (ds@(_:_),"") -> True
              _ -> False
          isDecToken :: String -> Bool
          isDecToken = all isDigit

          putImm, putReg, putMne :: String -> IO ()
          putImm = putBlue
          putReg = putCyan
          putMne = putYel
          putLbl = putWhite
          putError = putRed

          putNormal :: String -> IO ()
          putNormal = hPutStr h_oup
          putNormalC :: Char -> IO ()
          putNormalC = hPutChar h_oup

          putRed :: String -> IO ()
          putRed
            | color = hPutStrRed h_oup
            | otherwise = hPutStr h_oup
          putBlue :: String -> IO ()
          putBlue
            | color = hPutStrBlue h_oup
            | otherwise = hPutStr h_oup
          putCyan :: String -> IO ()
          putCyan
            | color = hPutStrCyan h_oup
            | otherwise = hPutStr h_oup
          putYel :: String -> IO ()
          putYel
            | color = hPutStrYellow h_oup
            | otherwise = hPutStr h_oup
          putMag :: String -> IO ()
          putMag
            | color = hPutStrMagenta h_oup
            | otherwise = hPutStr h_oup
          putGrn :: String -> IO ()
          putGrn
            | color = hPutStrGreen h_oup
            | otherwise = hPutStr h_oup
          putWhite :: String -> IO ()
          putWhite
            | color = hPutStrWhite h_oup
            | otherwise = hPutStr h_oup

          color :: Bool
          color = oColor os == ColorON

-- A+8*B-C => [["A"],["+"],["8","*","B"],["-"],["C"]]
splitTerms :: String -> [[String]]
splitTerms = loop
  where loop :: String -> [[String]]
        loop [] = []
        loop s =
          case span (`notElem`"-+") s of
            ("",op:sfx) -> [[op]] : loop sfx
            (term,"") -> [splitFactors term]
            (term,op:sfx) -> splitFactors term : [[op]] : loop sfx
splitFactors :: String -> [String]
splitFactors = loop
  where loop :: String -> [String]
        loop [] = []
        loop s =
          case span (`notElem`"*/") s of
            ("",op:sfx) -> [op] : loop sfx
            (fact,"") -> [fact]
            (fact,op:sfx) -> fact : [op] : loop sfx

